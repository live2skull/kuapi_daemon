import os
from os.path import join, dirname
from dotenv import load_dotenv
from sys import argv

def get_envirion(env_key, validate=True):
    val = os.environ.get(env_key)
    if val is None and validate is True: raise ValueError()
    return val

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

from celery import Celery

PIKA_HOST = get_envirion('pika_host')
PIKA_PORT = int(get_envirion('pika_port'))
PIKA_USER = get_envirion('pika_user')
PIKA_PASS = get_envirion('pika_pass')
PIKA_VIRTAL_HOST = get_envirion('pika_virtual_host')

AMQP_URL = 'amqp://%s:%s@%s:%s/%s' % (
        PIKA_USER, PIKA_PASS, PIKA_HOST, PIKA_PORT, PIKA_VIRTAL_HOST
)

app = Celery(backend=AMQP_URL, broker=AMQP_URL)

@app.task
def add(x, y):
    return x + y

if __name__ == '__main__':
    # celery 서버를 시작하고, amqp 브로커에 연결한다.
    app.worker_main(argv=argv)